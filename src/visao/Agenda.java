package visao;

import java.util.Scanner;

import controle.AgendaControle;
import modelo.Pessoa;

public class Agenda {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner leitor = new Scanner(System.in);
	// ---

		AgendaControle minhaAgenda = new AgendaControle();
		Pessoa umaPessoa;

	// ---

		System.out.println("Digite o nome:");
		String umNome = leitor.nextLine();
		umaPessoa = new Pessoa(umNome);


		System.out.println("Digite o telefone:");
		String umTelefone = leitor.nextLine();
		umaPessoa.setTelefone(umTelefone);

		System.out.println("Digite o email:");
		String umEmail = leitor.nextLine();
		umaPessoa.setEmail(umEmail);

		System.out.println(minhaAgenda.adicionar(umaPessoa));


	}

}
