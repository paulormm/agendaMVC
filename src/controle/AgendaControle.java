package controle;

import java.util.ArrayList;
import modelo.Pessoa;

public class AgendaControle {

	private ArrayList<Pessoa> listaPessoas;

	public AgendaControle(){
		listaPessoas = new ArrayList<Pessoa>();
	}

    public String adicionar(Pessoa umaPessoa) {
        String mensagem = "Pessoa adicionada com Sucesso!";

        listaPessoas.add(umaPessoa);
        return mensagem;
    }

    public void remover(Pessoa umaPessoa) {
        listaPessoas.remove(umaPessoa);
    }

    public Pessoa buscar(String umNome) {

        for (Pessoa umaPessoa: listaPessoas) {

        	String nomeAtual = umaPessoa.getNome();

            if (nomeAtual.equalsIgnoreCase(umNome)){
            	return umaPessoa;
            }
        }

        return null;
    }











}
